export interface TaskInterface {
  id: string;
  isCompleted: boolean;
  priority: number;
  effort: number;
  description: string;
  notes: string;
}

export default class Task implements TaskInterface {
  id: string;
  isCompleted: boolean;
  priority: number;
  effort: number;
  description: string;
  notes: string;

  constructor(taskData: TaskInterface) {
    this.id = taskData.id;
    this.isCompleted = taskData.isCompleted;
    this.priority = taskData.priority;
    this.effort = taskData.effort;
    this.description = taskData.description;
    this.notes = taskData.notes;
  }
}
