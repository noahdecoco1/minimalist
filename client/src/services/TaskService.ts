import { TaskInterface } from "@/models/task";

export const HIGHEST_PRIORITY = 4;
const PRIORITY_REGEX = new RegExp(`![0-${HIGHEST_PRIORITY}](?=\\s|$)`, "g");
export const HIGHEST_EFFORT = 5;
const EFFORTS_REGEX = new RegExp(`#[0-${HIGHEST_EFFORT}](?=\\s|$)`, "g");

class TaskService {
  extractDataFromText(text: string): TaskInterface {
    return {
      id: this.generateTaskId(),
      isCompleted: false,
      priority: this.extractDigitFromText(text, PRIORITY_REGEX),
      effort: this.extractDigitFromText(text, EFFORTS_REGEX),
      description: this.removePatternsFromText(text, [
        PRIORITY_REGEX,
        EFFORTS_REGEX
      ]),
      notes: ""
    };
  }

  private generateTaskId(): string {
    return new Date().getTime().toString();
  }

  private removePatternsFromText(text: string, regExps: RegExp[]): string {
    regExps.forEach((regExp: RegExp) => {
      text = text.replace(regExp, "");
    });
    return text.trim();
  }

  private extractDigitFromText(text: string, regExp: RegExp): number {
    const match = text.match(regExp);
    return match ? Number(match[0].slice(1)) : 0;
  }
}

export default new TaskService();
