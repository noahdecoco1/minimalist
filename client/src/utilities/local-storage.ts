class LocalStorage {
  private localStorage: Storage;

  constructor() {
    this.localStorage = window.localStorage;
  }

  getItem(key: string): any {
    const item = this.localStorage.getItem(key);
    return item ? JSON.parse(item) : null;
  }

  setItem(key: string, value: any): void {
    this.localStorage.setItem(key, JSON.stringify(value));
  }

  clear(): void {
    this.localStorage.clear();
  }
}

export default new LocalStorage();
